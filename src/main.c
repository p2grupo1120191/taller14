#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>

#define MAX 4096

/*Taller#14 Programacion Sistemas*/

void exec_command(char *line);

int main(int argc, char **argv){
	int status;
	char line[MAX] = {0};
	printf("$ ");
	while (fgets(line,MAX,stdin)) {
		if(fork()==0){
			exec_command(line);
		}else{
			wait(&status);
			printf("$ ");
		}

	}

	return 0;
}

void exec_command(char *line){
	char **args=calloc(3,sizeof(char *));
	int count=2;
	int current = 0;
	char *nl= strchr(line,'\n');
	if(nl!=NULL){
		*nl = '\0';
	}
	char *split=strchr(line,' ');
	char *previous=line;

	while(split!=NULL){
		if(current==count){
			count+=3;
			args=realloc(args,count*sizeof(char *));
		}
		*split='\0';
		split++;
		while(*split==' '){
			split++;
		}
		args[current++]=previous;
		previous=split;
		split=strchr(split,' ');
	}
	args[current]=previous;
	args[current+1] = (char *)NULL;
	execv(args[0],args);
	exit(1);
}
