
all:bin/terminal

bin/terminal: obj/main.o
	gcc -Wall -I./include $^ -o $@

obj/main.o: src/main.c
	gcc -Wall -I./include -c $< -o $@

.PHONY: clean
clean:
	rm bin/* obj/*.o
